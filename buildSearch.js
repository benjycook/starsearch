
var units = {worker: {prereqs: {minerals:50,supply:1}, buildTime:20}}
var buildings = { supplyDepot: {prereqs: {minerals:100}, buildTime: 40, supply: 8}}

function Node(parent,depth) {
    this.parent = parent
    this.state = parent.state
    this.prereqs = []
    this.discovered = false
    this.depth = depth;
    this.name = "root";
}

function WaitNode(parent,depth) {
	this.name = "wait";
    this.parent = parent
    this.state = JSON.parse(JSON.stringify( parent.state))
    this.prereqs = []
    this.depth = depth;
    if(this.state.units.worker) {
        this.state.minerals+= this.state.units.worker
    }
    
    if(this.state.factories) {
    	for(var f in this.state.factories) {
    		var factory = this.state.factories[f];
    		if(factory.training) {
    			factory.training.timeLeft-=1
    			if(factory.training.timeLeft<=0) {
    				
    				this.state.units[factory.training.unit]+=1;
    				factory.training = false;
    			}
    		}
    	}
    }
    if(this.state.construction) {
    	for(var b in this.state.construction) {
    		this.state.construction[b]-=1
    		var buildTime = this.state.construction[b];
    		if(buildTime<=0) {
    			delete this.state.construction[b];
    			if(this.state.buildings[b]) {
    				this.state.buildings[b]+=1;
    			} else {
    				this.state.buildings[b] = 1;
    			}
    			this.state.freeSupply+=buildings[b].supply;
    		} 
    	}
    }
    if(this.state.buildings) {
    	for(var b in this.state.buildings) {
    	    var building = buildings[b];
    	}
    }
    this.state.time+=1
}

function BuildUnitNode(parent,u,depth,f) {
	var unit = units[u];
	this.name = "build "+u;
    this.parent = parent
    this.depth = depth;
    this.state = JSON.parse(JSON.stringify( parent.state))
    this.prereqs = unit.prereqs
    this.state.minerals -= unit.prereqs.minerals;
    this.state.freeSupply -= unit.prereqs.supply;
    this.state.factories[f].training = {unit: u, timeLeft: units[u].buildTime};    
}

function BuildBuildingNode(parent,b,depth) {
	var building = buildings[b];
	this.name = "build "+b;
    this.parent = parent
    this.depth = depth;
    this.state = JSON.parse(JSON.stringify( parent.state))
    this.prereqs = building.prereqs
    this.state.minerals -= building.prereqs.minerals;
    this.state.construction[b] = building.buildTime;
}


var root = new Node({state: 
	{
		minerals: 5,
		freeSupply: 6,
		units: {worker:4},
		buildings : {},
		construction: {},
		factories: {command_center: {canBuild: ["worker"], training: false}},
		time:0
	}
},0);




function meetsGoal(node) {
	return (node.state.units.worker>=process.argv[2]);
}



function openNode(node) {
	var options = [];
	options.push(new WaitNode(node,node.depth+1));
	for(var f in node.state.factories) {
		var factory = node.state.factories[f];
		if(!factory.training) {
			for(var u in factory.canBuild) {
				var unitName = factory.canBuild[u];
				var buildUnit = new BuildUnitNode(node,factory.canBuild,node.depth+1,f);
				if(buildUnit.state.minerals<0) continue;
				if(buildUnit.state.freeSupply<0) continue;
				options.push(buildUnit);
				
			}
		}
		
	}
	for(var b in buildings) {
		// if we're not building anything (For now)
		if(node.state.construction[b]) break;
		var building = buildings[b];
		var buildBuilding = new BuildBuildingNode(node,b,node.depth+1);
		if(buildBuilding.state.minerals<0) continue;
		options.push(buildBuilding);
	}
	return options;
	
}


var bestOption;

var c = 0;
//for(var i=100;i<101;i++) {
	root.discovered = false;
	var stack = [root];
	var maxDepth = 500;
	while(stack.length) {
	    var node = stack.pop()
	    if(node.discovered) continue;
	    if(node.depth>=maxDepth) continue;
	    c++;
	    node.discovered = true;
	    if(meetsGoal(node)) {
	    	bestOption = node; break;
	    	if(bestOption) {
	    		if(bestOption.state.time>node.state.time) {
	    			bestOption = node;
	    		}
	    	} else {
	        	bestOption = node;
	    	}
	        continue;
	    }
	    stack = stack.concat(openNode(node))
	    if(c%50==0) {console.log(c,"nodes"); console.log(node.state.freeSupply,node.state.buildings)};
	}
	//if(bestOption) break;


if(!!!bestOption) {
	console.log("No result found, max depth is ",maxDepth);
} else {
	var fullSteps = [];
	while(bestOption.parent) {
		fullSteps.push(bestOption);
		bestOption = bestOption.parent;
	}
	var plan = fullSteps.reverse();
	var out = "";
	for(var i in plan) {
		out+=(plan[i].name)+",";//,plan[i].state);
	}
	console.log(out);
	console.log("plan length",i);
	console.log(plan[i].state);
}
console.log("nodes:",c);